/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "API", "index.html", [
    [ "Overview", "index.html", null ],
    [ "API Guide", "md_doc_APIGuide.html", [
      [ "Intended Audience", "md_doc_APIGuide.html#intendedaudience", null ],
      [ "Overview of the OpenSim API for Developers", "md_doc_APIGuide.html#overviewoftheapi", [
        [ "OpenSim’s Purpose", "md_doc_APIGuide.html#opensimpurpose", null ],
        [ "Design Philosophy", "md_doc_APIGuide.html#designphilosophy", null ],
        [ "Organization of OpenSim", "md_doc_APIGuide.html#organization", [
          [ "The Three Interface Layers of OpenSim Built on SimTK", "md_doc_APIGuide.html#autotoc_md51", null ],
          [ "A Composite Model Framework for Building a Computational System", "md_doc_APIGuide.html#autotoc_md52", null ]
        ] ],
        [ "A Simple Example", "md_doc_APIGuide.html#simpleexample", null ],
        [ "System and State", "md_doc_APIGuide.html#systemstate", null ],
        [ "Object", "md_doc_APIGuide.html#object", null ],
        [ "Component", "md_doc_APIGuide.html#component", [
          [ "Models are composed of Components", "md_doc_APIGuide.html#modelscomposed", null ],
          [ "Property", "md_doc_APIGuide.html#property", null ],
          [ "Socket", "md_doc_APIGuide.html#socket", null ],
          [ "Inputs and Outputs", "md_doc_APIGuide.html#inputsoutputs", null ]
        ] ],
        [ "Main Categories of Components", "md_doc_APIGuide.html#autotoc_md53", [
          [ "Operator", "md_doc_APIGuide.html#operator", null ],
          [ "Source", "md_doc_APIGuide.html#source", null ],
          [ "Reporter", "md_doc_APIGuide.html#reporter", null ],
          [ "ModelComponent", "md_doc_APIGuide.html#modelcomponent", null ]
        ] ],
        [ "Principal ModelComponents", "md_doc_APIGuide.html#modelcomponents", [
          [ "Frames", "md_doc_APIGuide.html#frames", null ],
          [ "Points, Stations and Markers", "md_doc_APIGuide.html#points", null ],
          [ "Joints", "md_doc_APIGuide.html#joints", null ],
          [ "Constraints", "md_doc_APIGuide.html#constraints", null ],
          [ "Forces", "md_doc_APIGuide.html#forces", null ],
          [ "Actuators", "md_doc_APIGuide.html#actuators", null ],
          [ "Controllers", "md_doc_APIGuide.html#controller", null ]
        ] ],
        [ "Data Handling Classes", "md_doc_APIGuide.html#datahandling", [
          [ "Data Table", "md_doc_APIGuide.html#autotoc_md54", null ],
          [ "Data Adapter", "md_doc_APIGuide.html#autotoc_md55", null ]
        ] ],
        [ "Solvers", "md_doc_APIGuide.html#solvers", null ]
      ] ],
      [ "Writing your own Component", "md_doc_APIGuide.html#writingcomponents", [
        [ "I. Constructing your Component’s Attributes", "md_doc_APIGuide.html#constructproperties", null ],
        [ "II. Finalize Properties and Connections of the Component", "md_doc_APIGuide.html#finalizefromproperties", null ],
        [ "III. Adding your Component’s Dynamics to the System", "md_doc_APIGuide.html#addtosystem", null ],
        [ "IV. Initializing and Recalling the State", "md_doc_APIGuide.html#initstatefromproperties", null ],
        [ "Test Components", "md_doc_APIGuide.html#testComponents", null ]
      ] ]
    ] ],
    [ "Moco", "mocomainpage.html", "mocomainpage" ],
    [ "OpenSim Copyright and License", "OpenSim_license_page.html", null ],
    [ "Class Groups", "modules.html", "modules" ],
    [ "Namespace Members", "namespacemembers.html", [
      [ "All", "namespacemembers.html", null ],
      [ "Functions", "namespacemembers_func.html", null ],
      [ "Variables", "namespacemembers_vars.html", null ],
      [ "Typedefs", "namespacemembers_type.html", null ],
      [ "Enumerations", "namespacemembers_enum.html", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List ", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", "functions_type" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"OpenSim_license_page.html",
"classOpenSim_1_1AbstractTool.html#a7b0bb0f397c9443424fb0c85e1986e74",
"classOpenSim_1_1Analysis.html#aee7676f4a1e868dcd45d7d75694e3212",
"classOpenSim_1_1BodyScale.html#ae5d8ef4bf2b1296d9493098eb688fc1f",
"classOpenSim_1_1CMC__Task.html#a803f0034aaab2916796446530fb6e186",
"classOpenSim_1_1Constant.html#a472bf4cf14f915404ed6cfd5433bccf8",
"classOpenSim_1_1ControlSet.html#a403c068c273166067c0a31e8a627d298",
"classOpenSim_1_1DataTable__.html#abf2d31940f02e0fb3e718f90b0832ab9",
"classOpenSim_1_1ExpressionBasedPointToPointForce.html#a3865b809d58a3fb5b1e50eab9f5ef24c",
"classOpenSim_1_1FunctionSet.html#ab29f51d2e14bd3c595364ed059569987",
"classOpenSim_1_1IO_1_1CwdChanger.html#a20be62290ea7f00ab0c2c86f045c1550",
"classOpenSim_1_1InverseKinematicsToolBase.html#a63d9f40907779ad4c247c2e52890843f",
"classOpenSim_1_1MarkerPair.html#ac08171cdcba92048f8a658127e936a4d",
"classOpenSim_1_1MocoContactTrackingGoal.html#a2a0f6e3ce662332a5f451edac70cd655",
"classOpenSim_1_1MocoInitialVelocityEquilibriumDGFGoal.html#a823e0c22206048b0d91260070ea8c1bf",
"classOpenSim_1_1MocoPhase.html#a34f6f43848b7996738af064f597a3736",
"classOpenSim_1_1MocoStudy.html#a75ed52d55bb1bafe1f0d3b778221faf0",
"classOpenSim_1_1ModOpAddReserves.html#ad25f37f006cda758aa8cf4979242be77",
"classOpenSim_1_1MultiplierFunction.html#aec688371ac626a7e9c5190551421615a",
"classOpenSim_1_1OrientationWeight.html#afceffe71dfdecfda59b97335298efa25",
"classOpenSim_1_1PointKinematics.html#a5881460b8376aedd18d2d1405d309fae",
"classOpenSim_1_1PropertyInt.html#a304b047d864c83a94f5fceee6865dbff",
"classOpenSim_1_1Property__Deprecated.html#a94ec9aacba0bc70b0dae40cfc26ecaa9acd9282250ed4f8c4584b512b904694b9",
"classOpenSim_1_1Set.html#a3caa97b3acc0b5d00ef40e4ce927cc13",
"classOpenSim_1_1StateVector.html#ac2536f3f60edf7eb8a45f7480ab79f42",
"classOpenSim_1_1Storage.html#a789505a75acc3f88e8b69425b8c879f7",
"classOpenSim_1_1TimestampLessThanEqualToPrevious.html#a3a979a3e0814cbfc7679042dff887270",
"classOpenSim_1_1WeldConstraint.html#af54ab15c605dc2565ca5307f954b90ff",
"group__reporters.html#gab43ef812e61ab4d6c2f852b7c0d98b9c"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';