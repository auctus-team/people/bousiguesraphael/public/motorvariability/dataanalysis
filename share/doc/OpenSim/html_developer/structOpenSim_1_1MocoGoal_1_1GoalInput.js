var structOpenSim_1_1MocoGoal_1_1GoalInput =
[
    [ "final_controls", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a50682463e77e4484137cfe4577f718e4", null ],
    [ "final_state", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a5ade07a20eb5670864e1e86abaccb6ec", null ],
    [ "final_time", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a71adacee31e13a88296ae8a2f2b0b736", null ],
    [ "initial_controls", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a687baaa80a876a5ab20ac5cfa672b541", null ],
    [ "initial_state", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a582425a9d4467009ec6c20c034e5e372", null ],
    [ "initial_time", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a7bc61f10e7c7c9da8379be3dc10d9746", null ],
    [ "integral", "structOpenSim_1_1MocoGoal_1_1GoalInput.html#a87e834f5e221a3461ca5ab8c88a326fd", null ]
];