/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "API", "index.html", [
    [ "Overview", "index.html", null ],
    [ "API Guide", "md_doc_APIGuide.html", [
      [ "Intended Audience", "md_doc_APIGuide.html#intendedaudience", null ],
      [ "Overview of the OpenSim API for Developers", "md_doc_APIGuide.html#overviewoftheapi", [
        [ "OpenSim’s Purpose", "md_doc_APIGuide.html#opensimpurpose", null ],
        [ "Design Philosophy", "md_doc_APIGuide.html#designphilosophy", null ],
        [ "Organization of OpenSim", "md_doc_APIGuide.html#organization", [
          [ "The Three Interface Layers of OpenSim Built on SimTK", "md_doc_APIGuide.html#autotoc_md51", null ],
          [ "A Composite Model Framework for Building a Computational System", "md_doc_APIGuide.html#autotoc_md52", null ]
        ] ],
        [ "A Simple Example", "md_doc_APIGuide.html#simpleexample", null ],
        [ "System and State", "md_doc_APIGuide.html#systemstate", null ],
        [ "Object", "md_doc_APIGuide.html#object", null ],
        [ "Component", "md_doc_APIGuide.html#component", [
          [ "Models are composed of Components", "md_doc_APIGuide.html#modelscomposed", null ],
          [ "Property", "md_doc_APIGuide.html#property", null ],
          [ "Socket", "md_doc_APIGuide.html#socket", null ],
          [ "Inputs and Outputs", "md_doc_APIGuide.html#inputsoutputs", null ]
        ] ],
        [ "Main Categories of Components", "md_doc_APIGuide.html#autotoc_md53", [
          [ "Operator", "md_doc_APIGuide.html#operator", null ],
          [ "Source", "md_doc_APIGuide.html#source", null ],
          [ "Reporter", "md_doc_APIGuide.html#reporter", null ],
          [ "ModelComponent", "md_doc_APIGuide.html#modelcomponent", null ]
        ] ],
        [ "Principal ModelComponents", "md_doc_APIGuide.html#modelcomponents", [
          [ "Frames", "md_doc_APIGuide.html#frames", null ],
          [ "Points, Stations and Markers", "md_doc_APIGuide.html#points", null ],
          [ "Joints", "md_doc_APIGuide.html#joints", null ],
          [ "Constraints", "md_doc_APIGuide.html#constraints", null ],
          [ "Forces", "md_doc_APIGuide.html#forces", null ],
          [ "Actuators", "md_doc_APIGuide.html#actuators", null ],
          [ "Controllers", "md_doc_APIGuide.html#controller", null ]
        ] ],
        [ "Data Handling Classes", "md_doc_APIGuide.html#datahandling", [
          [ "Data Table", "md_doc_APIGuide.html#autotoc_md54", null ],
          [ "Data Adapter", "md_doc_APIGuide.html#autotoc_md55", null ]
        ] ],
        [ "Solvers", "md_doc_APIGuide.html#solvers", null ]
      ] ],
      [ "Writing your own Component", "md_doc_APIGuide.html#writingcomponents", [
        [ "I. Constructing your Component’s Attributes", "md_doc_APIGuide.html#constructproperties", null ],
        [ "II. Finalize Properties and Connections of the Component", "md_doc_APIGuide.html#finalizefromproperties", null ],
        [ "III. Adding your Component’s Dynamics to the System", "md_doc_APIGuide.html#addtosystem", null ],
        [ "IV. Initializing and Recalling the State", "md_doc_APIGuide.html#initstatefromproperties", null ],
        [ "Test Components", "md_doc_APIGuide.html#testComponents", null ]
      ] ]
    ] ],
    [ "Moco", "mocomainpage.html", "mocomainpage" ],
    [ "OpenSim Copyright and License", "OpenSim_license_page.html", null ],
    [ "Class Groups", "modules.html", "modules" ],
    [ "Namespace Members", "namespacemembers.html", [
      [ "All", "namespacemembers.html", null ],
      [ "Functions", "namespacemembers_func.html", null ],
      [ "Variables", "namespacemembers_vars.html", null ],
      [ "Typedefs", "namespacemembers_type.html", null ],
      [ "Enumerations", "namespacemembers_enum.html", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List ", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"OpenSim_license_page.html",
"classOpenSim_1_1AbstractValueArray.html#a997100299b8858fa003981f5bdd1884d",
"classOpenSim_1_1AssemblySolver.html#a70661c035fa3e69b742bf524cfd82ed5",
"classOpenSim_1_1CMC__Task.html#a8b352549c71b7fc211377733f92c8b8d",
"classOpenSim_1_1Control.html#a6da87f2c2f82444e5c795ecb8bb6e728",
"classOpenSim_1_1DataQueueEntry__.html#abec74ca0627546f8c896af7779a150e4",
"classOpenSim_1_1FiberCompressiveForceCosPennationCurve.html",
"classOpenSim_1_1HuntCrossleyForce.html#a5d1b30e2587c16e205ce6974fcc20257",
"classOpenSim_1_1InverseKinematicsSolver.html#aabb1370b5bff9433295c5a72afaebfe4",
"classOpenSim_1_1MarkerWeight.html#a5d35d54744cc1030ccb1da329e018db8",
"classOpenSim_1_1MocoGoal.html#a4f55d8cf0ff70498be696061539440a5aa8838b07cec8fb4160a6d98271f70660",
"classOpenSim_1_1MocoPeriodicityGoal.html#a6c9758e13bc70d7428739c3695a6b558",
"classOpenSim_1_1MocoSumSquaredStateGoal.html#a79482807303307d8ebf1ec65fe6a703d",
"classOpenSim_1_1ModelScaler.html#a4851217b7b1735f49e59116f28fec37b",
"classOpenSim_1_1Output.html#a34bca3e481242522e57184d4ad79e99a",
"classOpenSim_1_1PropertyBool.html#a4c453e6d7f2d0b437b5006b6b8bed54f",
"classOpenSim_1_1PropertyStrArray.html#a4f2010c3cae9474c7f182b8a04818238",
"classOpenSim_1_1ScaleTool.html#a2304e85d15a9bd71d68fc7a51e181f15",
"classOpenSim_1_1StateVector.html#af79c1210b28b487ba9e0ae38e0a50574",
"classOpenSim_1_1StorageInterface.html#a6c6a474f6fd75b7901cc25c73a2ae07d",
"classOpenSim_1_1VectorFunction.html#a75c580513791d5a801270f85f5e2d3c0",
"group__commonutil.html#gab838826f9af13dc9371cf0a2e5ee81ed"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';