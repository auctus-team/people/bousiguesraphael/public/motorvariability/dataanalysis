# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        AssessActivity.py
# Purpose:     Calcul de score RULA - mouvement de pointage
#
# Author:      jonathan.savin
#
# Created:     28/09/2018
# Copyright:   (c) jonathan.savin 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
__version__ = "0.1"
__author__  = "J. Savin (INRS)"
__date__    = "2019-08-28"


from RULA import RULAprocessor as rula
import pandas as pd
import numpy as np
import logging, os
import btk
from Tkinter import Tk
from tkFileDialog import askdirectory,askopenfilename

import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from matplotlib.lines import Line2D as Line
rcParams['figure.figsize'] = 12, 5	# landscape

DEF_DIR = "H:/ICS-CPI/Recherche/EtudesRecherches/ET2014-2017 Variabilite/Valorisations/ManuscritThese2018/Memoire2018/Ressources/ChpActivPoint/Simulation/"
DEF_PST = "Postures.txt"
DEF_EEF = "RIND.txt"

PNT_START = 1000	 # début pointage
EXH_FRM   = 51000 #35600

DT = 1e-2

SEUIL_VIT_EVT = 0.15    # seuil de vitesse pour identifier les instants de contact

DIC_STOP = {"FM03":4,   # sujet FM03 : Borg 8 dépassé à 4 mn, mais manip poussée à t=5 => ne pas exploiter données postérieures à critère Borg
            "BP04":3,   # sujet BP04 : Borg 8 dépassé à 4 mn, mais manip poussée à t=6:14 => ne pas exploiter données postérieures à critère Borg
            "JB06":7,   # sujet BP04 : Borg 8 dépassé à 7 mn, mais manip poussée à t=8:35 => ne pas exploiter données postérieures à critère Borg
            "BC08":6}   # sujet BC08 : Borg 8 atteint à 6 mn, mais manip poussée à t=8:32 => ne pas exploiter données postérieures à critère Borg

#===============================================================================
# Classe de traitement des données de simulation XDE
#===============================================================================
class XDE_ErgoProcessor(object):
    """Lecture et traitement RULA des postures de pointage simulées par XDE.
    """

    #---------------------------------------------------------------------------
    def __init__( self,simuFil = "",withPlot = False):
        """Read data from input files (either txt files with simulated joints
        angles + end effector, or experimental MOCAP data) to build/read event
        data (target hits, cycles).
        """
        logging.info("Initializing data to be processed...")

        #--------------------
        # Find and read data
        #--------------------
        self.loadData()

        #----------------------
        # Build data processor
        #----------------------
        self.rulaProc = rula()
        self.rulaProc.setTask(False,False,True,False)    # interm, stat, repet, shock

        logging.info("Done !")

    #---------------------------------------------------------------------------
    def loadData(self):
        """Ask the user to select joint angles data then read/build events
        """
        rt=Tk()
        options = {}
        options['initialdir'] = DEF_DIR
        options['title'] = u"Sélectionner le fichier contenant les données d'angle :"
        options['filetypes'] = (("txt files","*.txt"),("mot files","*.mot"))
        simuFil = askopenfilename (**options)
        rt.destroy()
        logging.info("Data file : %s", simuFil)

            #--- read joint angles and events ---
        self.angFile = simuFil

        if simuFil.split(".")[-1] == "mot":
            # données expérimentales, chercher les events dans le fichier C3D "clean"
            self.getC3DPointingEvents()
            # calcul d'offsets liés à l'IK (reconstruction modèle OSIM,
            # possibilité rétroflexion du buste, à corriger)
            offset = self.getC3DstaticOffset()
            print(offset)
            self.ang_df = pd.read_csv( self.angFile,
                                       sep='\t',
                                       skiprows=10 )[:]
            for jnt in offset.keys():
                self.ang_df[jnt] -= offset[jnt]
            self.ang_df["Head_Flx"] -= self.ang_df["Head_Flx"].min()

        else:
            # Angles simulés XDE
            self.evts = self.getXDEPointingEvents()
            self.ang_df = pd.read_csv(self.angFile, sep='\t')[:][PNT_START:]

        self.nbCyc = (len(self.evts)-1) // 2  # le premier evt n'est pas "utile", puis // 2 pour nombre de cycles entiers
        logging.info("Nombre de cycle(s) complet(s) : %d", self.nbCyc)

        logging.info("Data found !")

    #---------------------------------------------------------------------------
    def getC3DPointingEvents(self):
        """Lecture des events déjà stockés dans le fichier C3D
        """
        subj = os.path.basename(self.angFile).split("-")[0]

        rt=Tk()
        options = {}
        options['initialdir'] = os.path.dirname(self.angFile)
        options['title'] = u"Sélectionner le fichier C3D du sujet %s :" % subj
        options['filetypes'] = (("C3D files","*.c3d"),)
        c3dFil = askopenfilename (**options)
        rt.destroy()

        reader = btk.btkAcquisitionFileReader()
        reader.SetFilename( str(c3dFil) )
        reader.Update()
        acq = reader.GetOutput()
        evtNbr = acq.GetEventNumber()
        evts = []
        for k in range(evtNbr):
            evt = acq.GetEvent( k )
            evts.append( evt.GetFrame() )

        # /!\ CAUTION : events added manually in MOKKA are inserted at the end
        # of the BTK collection ! Some may not be stored at the right place/order
        self.evts = sorted( evts )  # sort by frm

        if subj in DIC_STOP:
            # don't process after the specified duration
            lastFrm =   self.evts[0] \
                      + int(DIC_STOP[subj]*60*acq.GetPointFrequency())
            logging.debug("/!\ Last BORG compliant frame %d", lastFrm)
            lastEvt = np.where( np.array(self.evts) <= lastFrm )[0][-1]
            logging.debug("/!\ Stop processing at event %d, frm %d",
                          lastEvt, self.evts[lastEvt] )
            # drop next events...
            del self.evts[lastEvt+1:-1]

    #---------------------------------------------------------------------------
    def getC3DstaticOffset(self):
        """Read the "static.mot" corresponding to the "cnom.mot" and calculate
        return the mean values for uptrunk joints.
        """
        subj = os.path.basename(self.angFile).split("-")[0]
        motFile = os.path.join( os.path.dirname( self.angFile ),
                                subj+"-Static-clean-all.mot" )
        mot_df = pd.read_csv(motFile,sep='\t',skiprows=10)

        offs = dict()
        for key in ["UpTrunk_Flx", "UpTrunk_Lat", "UpTrunk_Rot",
                    "Head_Flx",    "Head_Lat",    "Head_Rot"]:
            offs[key] = mot_df[key].mean()

        return offs

    #---------------------------------------------------------------------------
    def getXDEPointingEvents(self):
        """Construction de la liste des événements de pointage
		# /!\ PAS AUSSI FACILE QU'EN MANIP ! pics mal détectés !
        #
        # Méthode :
		# 1- Identification des séquences de contact
		# 2- Recherche du min de la vitesse transversale dans ces zones
        """
        logging.info("Seeking contacts event(s)...")

        eefFile  = os.path.join( os.path.dirname(self.angFile),
                                 DEF_EEF )
        assert os.path.exists(eefFile),"Invalid position data file !"
        eef_df = pd.read_csv(eefFile,sep='\t')
        self.eefPos = np.array( eef_df[['X','Y','Z']][PNT_START:])

            #--- Process events ---
        logging.info("Building pointing events from End-effector positions file...")

        y = self.eefPos[:,1] # position sur l'axe Y, depuis PNT_START
#        y = self.eefPos[PNT_START:,1] # position sur l'axe Y, depuis PNT_START

            # Recherche des zones de contact, où vitesse longitudinale faible
        vel = np.abs(y[1:] - y[0:-1])/DT
        lowVel = np.where( vel < SEUIL_VIT_EVT )[0]
            # liste des indices de y où faible vitesse

            # Délimitation entre zones
        rup = (np.abs(lowVel[1:] - lowVel[0:-1]) > 1).tolist()
                  # tableau de booléens,
                  # False pendant une sequence de vitesse faible, True aux "transitions"

        nbZones = rup.count(True)	 # nbre de "sauts" séparant les zones
        logging.info("Found %d event(s)", nbZones)
        evts = np.zeros(nbZones,dtype=int)

        p = 0   # indice de la zone de contact proche (~ event)
        i = 0   # indice dans tableau 'rup'
        while p < nbZones:
            gapIdx = rup.index(True,i)	  # index du dernier point de la zone (le point suivant dans 'lowVel' n'est plus adjacent)
            seekMinZone = np.arange(lowVel[i], lowVel[gapIdx]+1)
            idxMin = np.argsort( vel[seekMinZone] )[0]   # recherche du vel_min sur la zone locale
            evts[p] = seekMinZone[idxMin]
            logging.debug("Evt(%03d) frame : %05d",p,evts[p])

            # lecture des frm des evts partant de PNT_START
            i = gapIdx+1	# gapIdx est absolu, pas relatif à i
            p += 1
                # Je ne m'occupe pas du type d'evt, je sais
                # qu'on commence par 'DST' en simu
        return evts

    #---------------------------------------------------------------------------
    def processCycle(self,cyc, withPlot = False):
        """Compute RULA scores on one cycle
        """
        beg = self.evts[2*cyc]      # cycle start frame number, from PNT_START
        end = self.evts[2*(cyc+1)]  # cycle end   frame number, from PNT_START
        logging.debug("Processing cycle %d [%05d-%05d]", cyc, beg, end)

        Ascores = np.zeros( (end-beg, 8))   # scores A1..A8
        Bscores = np.zeros( (end-beg, 7))   # scores B1..B7
        scoreR  = np.zeros( end-beg )       # score final

        for k in np.arange(end-beg):
            pst = self.ang_df[:].iloc[k+beg]
            self.rulaProc.setPosture(pst)
            Ascores[k,:] = self.rulaProc.getAScores()
            Bscores[k,:] = self.rulaProc.getBScores()
            scoreR[k]    = self.rulaProc.getFinalScore()

        if withPlot:
            f = plt.figure()
            f.suptitle("Cycle %d - frm[%05d-%05d]"%(cyc+1,beg,end))

            # RULA scores A
            ax=f.add_subplot(223)
            ax.set_title("Score A sur cycle")
            ax.grid("on")
            ax.bar( np.arange(8)+1, np.mean(Ascores,axis=0), align='center' )

            # RULA scores B
            ax=f.add_subplot(224)
            ax.set_title("Score B sur cycle")
            ax.grid("on")
            ax.bar( np.arange(7)+1, np.mean(Bscores,axis=0), align='center' )

##            # Distribution RULA scores A
##            ax=f.add_subplot(222)
##            ax.set_title("Distribution scores A sur cycle")
##            ax.grid("on")
##            max_Ascores = [6,3,4,2,9,1,3,13]
##            for sc,m in enumerate(max_Ascores):
##                for i in range(m):
##                    nb = len(np.where(Ascores[:,sc]==i+1)[0])
##                    # nbre de frames où Ascore vaut i (de 1 à 6, pas 0 à 5)
##                    rate = float(nb) / (end-beg)
##                    logging.debug("A%d = %d: rate %.2f", sc+1, i+1, rate*100)

            # joint angles
            ax=f.add_subplot(221)
            for k,dof in enumerate( ["RightArm_Flx", "RightArm_Abd", "RightArm_Rot",
                                     "RightForeArm_Flx", "RightForeArm_Prn",
                                     "RightHand_Flx", "RightHand_Abd",
                                     "UpTrunk_Flx","UpTrunk_Lat","UpTrunk_Rot",
                                     "Head_Flx", "Head_Lat", "Head_Rot"] ):
                lst = "-" if k < 7 else "--"
                ax.plot( np.arange(end-beg), self.ang_df[dof][beg:end],
                         ls = lst,
                         label = dof )
            ax.grid("on")
            ax.set_xlim(0,end-beg)
            lg = ax.legend(loc='best',fontsize = 11)
            lg.get_frame().set_alpha(0.5)

            plt.show()

        return np.mean(Ascores[:,7],axis=0), np.mean(Bscores[:,6],axis=0), np.mean(scoreR)

    #---------------------------------------------------------------------------
    def processAllCycles(self,withPlot = False):
        """Compute RULA scores on all cycles. Attention : stop when BORG
        threshold is reached !!!
        """

        a8 = np.zeros(self.nbCyc)
        b7 = np.zeros(self.nbCyc)
        fs = np.zeros(self.nbCyc)

        for k in np.arange(self.nbCyc):
            if not k % 10:
                logging.info("Processing cycle %d...",k)
            a8[k], b7[k], fs[k] = self.processCycle(k)

        self.a8 = a8
        self.b7 = b7
        self.fs = fs

        if withPlot:
            f = plt.figure()
            f.suptitle("RULA scores - %s" % (self.angFile[-50:]) )
            ax = f.add_subplot(111)
            ax.grid("on")
            ax.set_xlim(0,self.nbCyc)
            ax.set_xlabel("Cycle")
            ax.set_ylim(0,13)
            ax.set_ylabel("Score RULA")

            ax.plot(a8,label="score A8")
            ax.plot(b7,label="score B7")
##            ax.plot(fs,label="score final",ls='--')

            lg = ax.legend(loc='best')
            lg.get_frame().set_alpha(0.5)

            plt.show()

    #---------------------------------------------------------------------------
    def saveCycScores(self):
        """Save RULA scores for all cycles
        """
        savDir = os.path.dirname(self.angFile)
        savFil = os.path.basename(self.angFile)
        if savFil.split('.')[-1] == "mot":
            subj = savFil.split('-')[0]
        else:
            subj = "XDE"

        hdr = ("Sujet\tCycle\tCyclePC\tScoreA8\tScoreB7\tFinalScore\n")

        fil = os.path.join(savDir,subj+"-RULA_scores.txt")
        with open(fil,'w') as fh:
            fh.write(hdr)
            for k in np.arange(self.nbCyc):
                fh.write("%s\t%d\t%.2f\t%.2f\t%.2f\t%.2f\n" % (subj,
                                                     k,
                                                     float(k)/self.nbCyc,
                                                     self.a8[k],
                                                     self.b7[k],
                                                     self.fs[k] ) )
        fh.close()

        logging.info("RULA scores saved in %s", fil)

#===============================================================================
def main():
    xep = XDE_ErgoProcessor()

##    for cyc in [2,20,40,60,80]:
##        a8,b7,sf = xep.processCycle(cyc, True)
##        logging.info("Score A8    : %.1f",a8)
##        logging.info("Score B7    : %.1f",b7)
##        logging.info("Score final : %.1f",sf)
    xep.processAllCycles(withPlot=True)
    xep.saveCycScores()

#===============================================================================
if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG,
						format='%(asctime)s : %(levelname)s\t%(module)s::%(funcName)s (%(lineno)d) --- %(message)s',
						datefmt='%H:%M:%S')
	main()
