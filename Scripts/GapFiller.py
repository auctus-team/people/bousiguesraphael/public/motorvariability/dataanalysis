##################################################################################
# Libraries

from signal import pause
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sys

# Cheat Part: juste to debug this programm as a standalone : it doesn't affect the case when batch file is used
# Define subject and trial to compute
campaign_name = 'Mover202207'
subject = '5724'
trial = '0001'
sys.argv.append(campaign_name)
sys.argv.append(subject)
sys.argv.append(int(trial))

input_file = ""

# Listen .sh parameters
if(len(sys.argv) == 1):
    print("Please provide the campaign name, the subject name and the trial")
    exit()
elif(len(sys.argv) == 2):
    print("Please provide the subject name and the trial")
    exit()
elif(len(sys.argv) == 3):
    print("Please provide the trial")
    exit()
else:
    campaign_name = sys.argv[1]
    subject = sys.argv[2]
    trial = int(sys.argv[3])
    trial_str = f'{trial:04d}'

try:
    ##################################################################################
    ## Files
    # Input files
    # input_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'.trc'

    # Vérif part: holling some parts in files: RELB [994,1014], RWRA [750,770], HANS [1220,1240]
    input_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'.trc'

    # Output files
    output_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full.trc'
    gaps_to_fill_file = 'GapFillerList.csv'

    ##################################################################################
    # # In the experience MOVER n°1 (August 2022), the trial n°1 of the subject n°1543 shows a precision to the [cm] in the joint center reconstruction.
    # Let's consider this precision for our computation.
    # Though, we determine the maximum gap time to fill according to this precision and the theorical velocity of the movement (given by the handle center speed)
    v_human = 0.025 #[m/s]
    t_max_filling = 0.010 / v_human
    print("This programm will fill gaps of less than ", t_max_filling,' seconds')

    ##################################################################################
    # Open .trc file from Qualisys

    ## Extract Data from the .trc file and split them in multiple dataframes
    print("\nExtracting Data from .trc file: ", input_mocap_filename)
    # Extract first row of the .trc file
    print('- Extract 1st line')
    header_l1_trc = pd.read_csv(input_mocap_filename, nrows=1, header = None, delimiter='\t')

    # Extract parameter header
    print('- Extract Parameters')
    parameters = pd.read_csv(input_mocap_filename, nrows=1, header = 1, skiprows=0, delimiter='\t')

    # if data in .trc file are expressed in [mm], we have to convert joints center also in [mm] (multiply by 1000): the data from OSimModel.py are expressed in [m]
    unit_multiplier = 1 
    if parameters['Units'][0] =='mm': 
        unit_multiplier = 1000
        print('\tWARNING: Data in .trc file are expressed in [mm]')

    # Extract markers names of the .trc file
    print('- Extract markers names')
    markers_header = pd.read_csv(input_mocap_filename, nrows=1 , header = None ,skiprows=3) ## make just one celle tab --> use it only for export
    markers_trc = pd.read_csv(input_mocap_filename, nrows=2 , header = None , delimiter='\t',skiprows=3) # multi cells tab, but contains NaN values
    print("markers names\n", markers_trc)
    # Avoid NaN in the markers names dataframe, by replacinf NaN with the marker name
    for col_idx in range(markers_trc.shape[1]-1):
        if pd.isna(markers_trc.iloc[0,col_idx]) and pd.isna(markers_trc.iloc[0,col_idx+1]):
            markers_trc.iloc[0,col_idx] = markers_trc.iloc[0,col_idx + 2]
            markers_trc.iloc[0,col_idx+1] = markers_trc.iloc[0,col_idx + 2]

    print("markers names\n", markers_trc)
    # Extract dataframe containing coordinates of the markers
    print('- Extract data')
    data_trc = pd.read_csv(input_mocap_filename, header = 1, delimiter='\t',skiprows=3)

    ##################################################################################
    # Compute
    # print data dataframe

    print("Data:\n",data_trc)

    print("Copying dataframe")
    data_trc_filled = data_trc.copy()
except:
    input("Cannot open file n°"+str(trial)+" for subject "+str(subject))

##################################################################################
################################################################################## VALIDATED
# Test part with handmade dataframe
# test_df = pd.DataFrame({'Index':[0,1,2,3,4,5,6,7,8,9,10],'Time':[0,0.001,0.002,0.003,0.004,0.005,0.006,0.007,0.008,0.009,0.010]})
# test_df['Debut']=[0,0,0,0,1.25,1.28,1.32,1.56,1.47,1.84,1.49]
# test_df['Fin']=[1.56,1.47,1.84,1.49,1.25,1.28,1.32,0,0,0,0]
# test_df['Milieu']=[1.56,1.47,1.84,0,0,0,0,1.49,1.25,1.28,1.32]
# data_trc_filled = test_df.copy()
##################################################################################
##################################################################################

# print("Copy:\n",data_trc_filled)

##################################################################################
# FUNCTIONS
##################################################################################

def FillGap(data_col, i0, iN):
    data_filled = data_col.copy()
    if i0 ==0 and iN == data_filled.shape[0]-1:
        print("ERROR: data is empty !")
    elif i0 == 0: # Gap at the begining of a trajectory # A PRIORI USELESS WHEN USING POSTGAPFILLING.PY IN A SECOND PHASE
        a = data_filled[iN+2] - data_filled[iN+1] # Compute the growth rate
        for i in range(i0, iN+1):
            data_filled[i] = data_filled[iN+1] - a * (iN + 1 - i)
    elif iN == data_filled.shape[0]-1: # Gap at the end of a trajectory # A PRIORI USELESS WHEN USING POSTGAPFILLING.PY IN A SECOND PHASE
        a = data_filled[i0 - 1] - data_filled[i0 - 2]
        for i in range(i0, iN+1):
            data_filled[i] = data_filled[i0 - 1] + a * (i-i0+1)
    else: # Gap in the middle of a trajectory
        delta = data_filled[iN+1] - data_filled[i0 - 1] # Compute the Step to fill
        a = delta / (iN - i0 + 2)# Compute the growth rate from the step
        for i in range(i0, iN+1):
            data_filled.iloc[i] = data_filled.iloc[i0 - 1] + a * (i-i0+1)

    return data_filled

def GapTooLarge(campaign_name,subject,trial_str, marker, start, end,gaps_to_fill_file):
    print("\n##############")
    print("Subject n°",subject,'\tTrial n°',trial_str)
    print("Gap too large to fill: please correct manually the gap on marker ", markers_trc.iloc[0, col_idx], ' from frame ', data_trc_filled.iloc[idx_0,0], ' to frame ', data_trc_filled.iloc[idx_N,0])
    print("##############")
    print("Adding to GapFillerList.csv ...")
    gaptofill_df = pd.DataFrame({"Campaign":[campaign_name], "Subject":[subject], "Trial":[trial_str], "Marker":[marker], "StartFrame":[start], "EndFrame":[end]})
    gaptofill_df.to_csv(gaps_to_fill_file, mode="a", index=False, sep=';', header=False) # mode "a" means to append dataframe in the .csv file

##################################################################################
# MAIN
##################################################################################

try:

    # Gaps analyzer
    print("Columns: ", data_trc_filled.columns)
    columns_to_fill = data_trc_filled.columns[data_trc_filled.columns!='Unnamed: 0']
    columns_to_fill = columns_to_fill[columns_to_fill!='Unnamed: 1']
    col_idx = 2 # the first index begins at 2 because of the removing of Unnamed 0 and Unnamed 1
    print("Columns to fill : ", columns_to_fill)
    for col in columns_to_fill: # Check each column one by one, if it contains 0 values (corresponds to NaN values in Qualisys)
        print("\nColumn name:",col)
        empty_data = np.where(data_trc_filled[col]==0)
        if len(empty_data[0] > 0 ):
            print("empty_data in the column:", empty_data)
            idx_0 = empty_data[0][0]
            idx_N = idx_0
            for i in range(len(empty_data[0])):
                if (empty_data[0][i] - idx_N) == 1 : # check if the values are consecutive
                    idx_N = empty_data[0][i]
                elif (empty_data[0][i] - idx_N) > 1  : # if not consecutive values, fil the previous gap, and restart a new block
                    # Verify is this gap is fillable
                    t_to_fill = data_trc_filled.iloc[idx_N,1] -  data_trc_filled.iloc[idx_0,1] # 'Time' column is the 2nd column (index 1) in a .trc file
                    print("Time to fill = ",t_to_fill)
                    if t_to_fill < t_max_filling:
                        print("Filling Processing")
                        data_trc_filled[col] = FillGap(data_trc_filled[col],idx_0, idx_N) # Fill the gap
                        print(data_trc_filled[col]) # Fill the gap
                    else: # Impossible to fill
                        marker_fail = markers_trc.iloc[0, col_idx]
                        start_fail = data_trc_filled.iloc[idx_0,0]
                        end_fail = data_trc_filled.iloc[idx_N,0]
                        GapTooLarge(campaign_name,subject,trial_str, marker_fail, start_fail, end_fail, gaps_to_fill_file)
                        # input("\nPlease Confirm by pressing any button\t")
                    idx_0 = empty_data[0][i] # restart a new block with 0 values
                    idx_N = idx_0 # restart a new block with 0 values
            # For the last element, take the block and fill it !
            # Verify is this gap is fillable
            t_to_fill = data_trc_filled.iloc[idx_N,1] -  data_trc_filled.iloc[idx_0,1] # 'Time' column is the 2nd column (index 1) in a .trc file
            print("Time to fill = ",t_to_fill)
            if t_to_fill < t_max_filling:
                print("Filling Processing - Last one in this column")
                data_trc_filled[col] = FillGap(data_trc_filled[col],idx_0, idx_N) # Fill the gap
                print(data_trc_filled[col]) # Fill the gap
            else: # Impossible to fill
                marker_fail = markers_trc.iloc[0, col_idx]
                start_fail = data_trc_filled.iloc[idx_0,0]
                end_fail = data_trc_filled.iloc[idx_N,0]
                GapTooLarge(campaign_name,subject,trial_str, marker_fail, start_fail, end_fail, gaps_to_fill_file)
                # input("\nPlease Confirm by pressing any button\t")
        else: 
            print("No gap to fill")       
        col_idx += 1


    ##################################################################################
    # Save new .trc file

    # Replace Dirty columns names by null string ''
    data_trc_filled.rename(columns={'Unnamed: 0':'','Unnamed: 1':''}, inplace=True) 

    ## Reconsitute the .trc file
    # Concatenate in the right order the 4 dataframes to obtain the .trc file like the original one
    print('\nSaving the new .trc file...')
    header_l1_trc.to_csv(output_mocap_filename, index=False, sep='\t', header=False)
    parameters.to_csv(output_mocap_filename, mode="a", index=False, sep='\t', header=True) # mode "a" means to append dataframe in the .csv file
    #  Fill the header markers names
    markers_header.to_csv(output_mocap_filename, mode="a", index=False, sep=' ', header=False) # a simple ' ' [space] string works perfectly to transform the 1 cell dataframe into a multi-cells csv file (the \t are kept in the one cell df)
    # Fill the dataframe with new values
    data_trc_filled.to_csv(output_mocap_filename, mode="a", index=False, sep='\t', header=True)
    print('\t-> Data saved in ',output_mocap_filename)
except:
    input("Problem in the Main of GapFiller.py, trial n°"+ str(trial)+" subject n°"+str(subject))



