import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_theme()


from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

from scipy.spatial import ConvexHull

import os, sys

import StoDF

########################################

s_speed = 0.025 #[m/s]
wireloop_length = 0.48 #[m]
 
########################################

# Cheat Part: just to debug this programm as a standalone 
# Define subject and trial to compute
campaign_name = 'Mover202207'
subject = '1543'
trial = 1
# trial_list = [1,7,15,23,36,48]
s_round=1
s_to_plot=np.arange(start=0.20, stop=0.81, step=10**(0-s_round)) # Depends on s_round: step = 0.1 if s_round==1, step = 0.003 if s_round==3
s_to_plot = s_to_plot.round(s_round)
print(s_to_plot)
# trial_list = [2,12,19,27,34,42,50]
# trial_list = np.concatenate((np.arange(start=1, stop=39),np.arange(start=40, stop=51)))
trial_list = np.concatenate((np.arange(start=1, stop=40),np.arange(start=40, stop=51)))
# subject_list = ['4734']
subject_list = ['1543','2682','3549','4734','5165', '5724', '6726', '6843', '7961']
# Group the trials in one dataframe
group_df_full=pd.DataFrame()
for subject in subject_list:
    group_df=pd.DataFrame()
    break_flag=False
    for trial in trial_list:
        # Create Dataframe for specific s values
        rt_df_s_trial = pd.DataFrame()

        # print("Trial:", trial)
        sys.argv=['']
        sys.argv.append(campaign_name)
        sys.argv.append(subject)
        sys.argv.append(int(trial))

        input_file = ""

        # Listen .sh parameters
        if(len(sys.argv) == 1):
            print("Please provide the campaign name, the subject name and the trial")
            exit()
        elif(len(sys.argv) == 2):
            print("Please provide the subject name and the trial")
            exit()
        elif(len(sys.argv) == 3):
            print("Please provide the trial")
            exit()
        else:
            campaign_name = sys.argv[1]
            subject = sys.argv[2]
            trial = int(sys.argv[3])
            trial_str = f'{trial:04d}'

        try:

            ##################################################################################
            # Folders

            campaign_folder = '/home/raphael/Documents/MOVER/Data/'+campaign_name
            rt_folder = campaign_folder+'/RT/'

            ##################################################################################
            # Import Data
            print('Importing Data...')
            rt_file_path = rt_folder+subject+'/'+trial_str+'_RT.csv'
            rt_df = pd.read_csv(rt_file_path)
            print('\t-> Data imported from ',rt_file_path)

            rt_df_trial = rt_df.copy()

            ##################################################################################
            # Transform dataframe for s_to_plot
            for s_i in s_to_plot:
                rt_df_trial_s_i = StoDF.s_to_df(s_i,rt_df_trial)
                if rt_df_trial_s_i.empty :  # if the trial does not contains the abscissa we need, forget this trial
                    break_flag=True
                    break
                rt_df_s_trial = pd.concat([rt_df_s_trial,rt_df_trial_s_i], ignore_index=True)

            if break_flag==False: # if the trial does not contains the abscissa we need, forget this trial
                break_flag=False
                print("data ok")
                ##################################################################################
                # Add Theoretical curv abscissa and error columns
                rt_df_s_trial['S_theo']=(rt_df_s_trial['Time']-rt_df_s_trial['Time'][0]) * s_speed + rt_df_s_trial['s'][0]
                rt_df_s_trial['s_error'] = rt_df_s_trial['S_theo'] - rt_df_s_trial['s']

                ##################################################################################
                # Add Speed column
                delta_s = rt_df_s_trial['s'].diff()
                delta_t = rt_df_s_trial['Time'].diff()
                delta_p = delta_s * wireloop_length
                v = delta_p / delta_t
                rt_df_s_trial['v'] = v

                # print("rt_df_s_trial=\n", rt_df_s_trial)
                ##################################################################################
                # Write trial no and add in group dataframe 

                rt_df_s_trial['Trial']=rt_df_s_trial.iloc[:,0]*0 + trial
                group_df = pd.concat([group_df, rt_df_s_trial], ignore_index=True)

                # print("Group DF=\n", group_df)
            else:
                # input('broken')
                print('broken')
                break_flag=False
        except:
            print("ERROR for trial n°", trial)
    group_df['Subject']=""+subject
    print("Group DF PIN=\n", group_df)
    group_df_full = pd.concat([group_df_full, group_df], ignore_index=True)

# Transform radians into degrees
group_df_full['SwivelAngle'] = group_df_full['SwivelAngle'] * 180 / np.pi
group_df_full['TwistAngle'] = group_df_full['TwistAngle'] * 180 / np.pi
# group_df_full['Subject'] = group_df_full['Subject'].apply(int).apply(str) # print subject name as integer (without comma) and as a name (a string so)
# group_df_full['Subject']= 'subject_'+group_df_full['Subject'] 

# Round s values
group_df_full['s'] = group_df_full['s'].round(s_round)

# Keep only data for S in s_to_plot
# group_df_full = group_df_full[group_df_full['s'].isin(s_to_plot)]

print("Group DF=\n", group_df_full)



##################################################################################
# Plot Data
print('Plotting Data...')

cmap_list = ['Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
                    'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
                    'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn']

# # 3D plot is not really readable
# figure3D = plt.figure(figsize=[4,4])
# ax3d_1 = figure3D.add_subplot(projection='3d')
# ax3d_1.plot(rt_df['s'],rt_df['SwivelAngle']*180/np.pi,rt_df['TwistAngle']*180/np.pi)
# ax3d_1.set_xlabel('s')
# ax3d_1.set_ylabel('Swivel Angle (°)')
# ax3d_1.set_zlabel('Twist Angle (°)')

# plt.savefig("test.pdf")
# rt_df_to_sns=rt_df.copy()
# hue_name = 's - Trial n°'+trial_str
# rt_df_to_sns.rename(columns={'s':hue_name}, inplace=True)

##################################################################################
# fig1 = plt.figure(figsize=[4,4])
# ax11 = fig1.add_subplot(111)
# ax11 = sns.scatterplot(data=group_df , x='SwivelAngle', y='TwistAngle', hue='s', style='Trial', palette=mpl.colormaps['cool'], legend='brief',edgecolor="none")
# ax11.set_xlabel('Swivel Angle (°)')
# ax11.set_ylabel('Twist Angle (°)')
# ax11.set_title('Redundancy Tube of subject n°'+subject)

####################################################################################################################################################################
####################################################################################################################################################################
####################################################################################################################################################################
#  Redundancy Tubes
####################################################################################################################################################################
rt_s_02 = group_df_full[group_df_full['s']==0.2].reset_index(drop=True)
rt_s_04 = group_df_full[group_df_full['s']==0.4].reset_index(drop=True)
rt_s_05 = group_df_full[group_df_full['s']==0.5].reset_index(drop=True)
rt_s_06 = group_df_full[group_df_full['s']==0.6].reset_index(drop=True)
rt_s_08 = group_df_full[group_df_full['s']==0.8].reset_index(drop=True)

print("rt_s_05=\n", rt_s_05)
# print("TEST:", rt_s_05[rt_s_05['Subject']==1543])
# rt_s_05 = rt_s_05.sort_values('Subject')
figrt = plt.figure("Redundancy Tube Slices", figsize=[4,4] )
####################################################################################################################################################################
# s=0.2
axrt2 = figrt.add_subplot(221)
axrt2 = sns.scatterplot(data=rt_s_02 , x='SwivelAngle', y='TwistAngle' , hue='Subject',  legend='full',edgecolor="none")
axrt2.set_xlabel('Swivel Angle (°)')
axrt2.set_ylabel('Twist Angle (°)')
axrt2.set_title('Redundancy Tube Slice for s= 0.2')
axrt2.axis('equal')
# Plot convex hulls
colors_list = ["blue", "orange", "green","red","purple", "brown", "pink", "grey", "gold"]
i=0
for subject in subject_list:
    points = rt_s_02[rt_s_02['Subject']==subject][["SwivelAngle","TwistAngle"]].values
    hull = ConvexHull(points)
    for simplex in hull.simplices:
        axrt2.plot(points[simplex, 0], points[simplex, 1], color=colors_list[i], linestyle="-", linewidth =4)
    i+=1

####################################################################################################################################################################
# s=0.4
axrt4 = figrt.add_subplot(222)
axrt4 = sns.scatterplot(data=rt_s_04 , x='SwivelAngle', y='TwistAngle' , hue='Subject',  legend='full',edgecolor="none")
axrt4.set_xlabel('Swivel Angle (°)')
axrt4.set_ylabel('Twist Angle (°)')
axrt4.set_title('Redundancy Tube Slice for s= 0.4')
axrt4.axis('equal')
# Plot convex hulls
i=0
for subject in subject_list:
    points = rt_s_04[rt_s_04['Subject']==subject][["SwivelAngle","TwistAngle"]].values
    hull = ConvexHull(points)
    for simplex in hull.simplices:
        axrt4.plot(points[simplex, 0], points[simplex, 1], color=colors_list[i], linestyle="-", linewidth =4)
    i+=1

####################################################################################################################################################################
# s=0.6
axrt6 = figrt.add_subplot(223)
axrt6 = sns.scatterplot(data=rt_s_06 , x='SwivelAngle', y='TwistAngle' , hue='Subject',  legend='full',edgecolor="none")
axrt6.set_xlabel('Swivel Angle (°)')
axrt6.set_ylabel('Twist Angle (°)')
axrt6.set_title('Redundancy Tube Slice for s= 0.6')
axrt6.axis('equal')
# Plot convex hulls
i=0
for subject in subject_list:
    points = rt_s_06[rt_s_06['Subject']==subject][["SwivelAngle","TwistAngle"]].values
    hull = ConvexHull(points)
    for simplex in hull.simplices:
        axrt6.plot(points[simplex, 0], points[simplex, 1], color=colors_list[i], linestyle="-", linewidth =4)
    i+=1

####################################################################################################################################################################
# s=0.8
axrt8 = figrt.add_subplot(224)
axrt8 = sns.scatterplot(data=rt_s_08 , x='SwivelAngle', y='TwistAngle' , hue='Subject',  legend='full',edgecolor="none")
axrt8.set_xlabel('Swivel Angle (°)')
axrt8.set_ylabel('Twist Angle (°)')
axrt8.set_title('Redundancy Tube Slice for s= 0.8')
axrt8.axis('equal')
# Plot convex hulls
i=0
for subject in subject_list:
    points = rt_s_08[rt_s_08['Subject']==subject][["SwivelAngle","TwistAngle"]].values
    hull = ConvexHull(points)
    for simplex in hull.simplices:
        axrt8.plot(points[simplex, 0], points[simplex, 1], color=colors_list[i], linestyle="-", linewidth =4)
    i+=1

# Adjust subplots placement
plt.subplots_adjust(left=0.1,
                    bottom=0.1,
                    right=0.9,
                    top=0.9,
                    wspace=0.4,
                    hspace=0.4)

#  Adjust plot bounds
axrt2.set_aspect('auto')
axrt4.set_aspect('auto')
axrt6.set_aspect('auto')
axrt8.set_aspect('auto')

rt_xmax = 75
rt_xmin = 10
rt_ymax = 80
rt_ymin = -15

axrt2.set_xlim(rt_xmin,rt_xmax)
axrt2.set_ylim(rt_ymin,rt_ymax)
axrt4.set_xlim(rt_xmin,rt_xmax)
axrt4.set_ylim(rt_ymin,rt_ymax)
axrt6.set_xlim(rt_xmin,rt_xmax)
axrt6.set_ylim(rt_ymin,rt_ymax)
axrt8.set_xlim(rt_xmin,rt_xmax)
axrt8.set_ylim(rt_ymin,rt_ymax)
#####################################################
# Plot every data - hue = subjects
sns.pairplot(data=group_df_full, hue='Subject')#,  legend='full')#,edgecolor="none") # palette=mpl.colormaps['tab10'],


#####################################################
# Plot every data - hue = s - subject = 15436
# groud_df_subject = group_df_full[group_df_full['Subject']=='1543']
# sns.pairplot(data=groud_df_subject, hue='s')


##############################################################
# Idea n°1

# fig1 = plt.figure("Idea 1", figsize=[4,4])

# ax10 = fig1.add_subplot(221)
# ax10.set_title("Idea 1 - "+subject_list[0])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[0]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='SwivelAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax10.legend()

# ax11 = fig1.add_subplot(222)
# ax11.set_title("Idea 1 - "+subject_list[1])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[1]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='SwivelAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax11.legend()

# ax12 = fig1.add_subplot(223)
# ax12.set_title("Idea 1 - "+subject_list[2])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[2]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='SwivelAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax12.legend()

# ax13 = fig1.add_subplot(224)
# ax13.set_title("Idea 1 - "+subject_list[3])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[3]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='SwivelAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax13.legend()

##############################################################
# # Idea n°2

# fig2 = plt.figure("Idea 2", figsize=[4,4])

# ax20 = fig2.add_subplot(221)
# ax20.set_title("Idea 2 - "+subject_list[0])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[0]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='TwistAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax20.legend()

# ax21 = fig2.add_subplot(222)
# ax21.set_title("Idea 2 - "+subject_list[1])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[1]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='TwistAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax21.legend()

# ax22 = fig2.add_subplot(223)
# ax22.set_title("Idea 2 - "+subject_list[2])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[2]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='TwistAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax22.legend()

# ax23 = fig2.add_subplot(224)
# ax23.set_title("Idea 2 - "+subject_list[3])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[3]]
# sns.scatterplot(data=group_df_subject, x= 'Trial',y='TwistAngle', hue='s',palette=mpl.colormaps['gist_rainbow'])
# ax23.legend()



##############################################################
# Idea n°3

fig3 = plt.figure("Idea 3", figsize=[4,4])
ax31 = fig3.add_subplot(111)
ax31.set_title("Idea 3")
group_df_subject = group_df_full[group_df_full['Subject']=='1543']
sns.scatterplot(data=group_df_subject, x= 's',y='TwistAngle', hue='Trial',palette=mpl.colormaps['gist_rainbow'])

# # STD
# fig3a = plt.figure("Idea 3 - STD", figsize=[4,4])
# ax3a1 = fig3a.add_subplot(111)
# ax3a1.set_title("STD")
# std_df = pd.DataFrame(columns=['s','STD Twist','Subject'])
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     for s in s_to_plot:
#         # s=s.round(s_round)
#         s_df = group_df_subject[group_df_subject['s']==s]
#         std = np.std(s_df['TwistAngle'])
#         df = pd.DataFrame({'s':[s],'STD Twist':[std], 'Subject':[subject]})
#         std_df = pd.concat([std_df, df], ignore_index=True)
#         print("s = ",s,"\tstd = ",std)
#     print("STD",std_df)
# # ax3a1.plot(std_df)
# sns.lineplot(data=std_df, x='s',y='STD Twist',hue='Subject')

# # Mean
# fig3b = plt.figure("Idea 3 - Mean", figsize=[4,4])
# ax3b1 = fig3b.add_subplot(111)
# ax3b1.set_title("Mean")
# mean_df = pd.DataFrame(columns=['s','Mean Twist','Subject'])
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     for s in s_to_plot:
#         # s=s.round(s_round)
#         s_df = group_df_subject[group_df_subject['s']==s]
#         mean = np.mean(s_df['TwistAngle'])
#         df = pd.DataFrame({'s':[s],'Mean Twist':[mean], 'Subject':[subject]})
#         mean_df = pd.concat([mean_df, df], ignore_index=True)
#         print("s = ",s,"\tmean = ",mean)
#     print("Mean",mean_df)
# # ax3b1.plot(mean_df)
# sns.lineplot(data=mean_df, x='s',y='Mean Twist',hue='Subject')

# Combine mean and STD in lineplot
fig3c = plt.figure("Twist angle mean values with STD", figsize=[4,4])
ax3c1 = fig3c.add_subplot(111)
ax3c1.set_title("Mean Twist Angle with with STD area ")
sns.lineplot(data=group_df_full, x= 's',y='TwistAngle', hue='Subject', errorbar='sd')
ax3c1.set_xlabel("s")
ax3c1.set_ylabel("Mean Twist angle (°)")

# # Combine mean and STD in boxplot
# fig3d = plt.figure("Idea 3 - Boxplot", figsize=[4,4])
# ax3d1 = fig3d.add_subplot(111)
# ax3d1.set_title("Mean Twist Angle with with STD boxes ")
# sns.boxplot(data=group_df_full, x= 's',y='TwistAngle', hue='Subject')
# ax3d1.set_xlabel("s")
# ax3d1.set_ylabel("Twist angle (°)")

# # Mean with boxes
# fig3e = plt.figure("Idea 3 - Mean with boxes", figsize=[4,4])
# ax3e1 = fig3e.add_subplot(111)
# ax3e1.set_title("Mean Twist Angle with with STD boxes")
# sns.boxplot(data=group_df_full, x= 's',y='TwistAngle', hue='Subject')
# mean_df = pd.DataFrame(columns=['s','Mean Twist','Subject'])
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     for s in s_to_plot:
#         # s=s.round(s_round)
#         s_df = group_df_subject[group_df_subject['s']==s]
#         mean = np.mean(s_df['TwistAngle'])
#         df = pd.DataFrame({'s':[s],'Mean Twist':[mean], 'Subject':[subject]})
#         mean_df = pd.concat([mean_df, df], ignore_index=True)
#         print("s = ",s,"\tmean = ",mean)
#     print("Mean",mean_df)

# sns.pointplot(data=mean_df, x= 's',y='Mean Twist', hue="Subject")
# ax3e1.set_xlabel("s")
# ax3e1.set_ylabel("Mean Twist angle (°)")




##############################################################
# # Idea n°4

# fig4 = plt.figure("Idea 4", figsize=[4,4])
# ax41 = fig4.add_subplot(111)
# ax41.set_title("Idea 4")
# # group_df_subject = group_df_full[group_df_full['Subject']=='1543']
# sns.scatterplot(data=group_df_full, x= 'v',y='SwivelAngle',palette=mpl.colormaps['gist_rainbow'], hue='Trial', legend='full')

##############################################################
# # Idea n°5 

# fig5 = plt.figure("Idea 5", figsize=[4,4])
# ax51 = fig5.add_subplot(211)
# ax51.set_title("Idea 5")
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     sns.regplot(data=group_df_subject, x= 's_error',y='SwivelAngle', label=subject)
# ax51.legend()
# ax52 = fig5.add_subplot(212)
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     sns.regplot(data=group_df_subject, x= 's',y='s_error', label=subject)
# ax52.legend()

##############################################################
# Idea n°6

fig6 = plt.figure("Swivel Angle Distribution", figsize=[4,4])
ax61 = fig6.add_subplot(111)
ax61.set_title("Swivel Angle Distribution")
for subject in subject_list:
    group_df_subject = group_df_full[group_df_full['Subject']==subject]
    sns.histplot(data=group_df_subject, x= 'SwivelAngle', stat='percent', kde=True, label=subject, fill=True, binwidth=1)
ax61.legend(title="Subject")
ax61.set_xlabel("Swivel angle (°)")
ax61.set_ylabel("Occurrence (%)")
# INDIVIDUAL
# fig6b = plt.figure("Idea 6 BIS", figsize=[4,4])

# ax6b0 = fig6b.add_subplot(221)
# ax6b0.set_title("Idea 6"+subject_list[0])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[0]]
# sns.histplot(data=group_df_subject, x= 'SwivelAngle', stat='percent', kde=True, label=subject_list[0], fill=True, binwidth=1)
# ax6b0.legend()

# ax6b1 = fig6b.add_subplot(222)
# ax6b1.set_title("Idea 6"+subject_list[1])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[1]]
# sns.histplot(data=group_df_subject, x= 'SwivelAngle', stat='percent', kde=True, label=subject_list[1], fill=True, binwidth=1)
# ax6b1.legend()

# ax6b2 = fig6b.add_subplot(223)
# ax6b2.set_title("Idea 6"+subject_list[2])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[2]]
# sns.histplot(data=group_df_subject, x= 'SwivelAngle', stat='percent', kde=True, label=subject_list[2], fill=True, binwidth=1)
# ax6b2.legend()

# ax6b3 = fig6b.add_subplot(224)
# ax6b3.set_title("Idea 6"+subject_list[3])
# group_df_subject = group_df_full[group_df_full['Subject']==subject_list[3]]
# sns.histplot(data=group_df_subject, x= 'SwivelAngle', stat='percent', kde=True, label=subject_list[3], fill=True, binwidth=1)
# ax6b3.legend()


##############################################################
# Idea n°7

fig7 = plt.figure("Twist Angle Distribution", figsize=[4,4])
ax71 = fig7.add_subplot(111)
ax71.set_title("Twist Angle Distribution")
for subject in subject_list:
    group_df_subject = group_df_full[group_df_full['Subject']==subject]
    sns.histplot(data=group_df_subject, x= 'TwistAngle', stat='percent', kde=True, label=subject, fill=True, binwidth=1)
ax71.legend(title="Subject")
ax71.set_xlabel("Twist angle (°)")
ax71.set_ylabel("Occurrence (%)")

##############################################################
# Idea n°8

fig8 = plt.figure("Idea 8", figsize=[4,4])
ax81 = fig8.add_subplot(111)
ax81.set_title("Idea 8")
group_df_subject = group_df_full[group_df_full['Subject']=='1543']
sns.scatterplot(data=group_df_subject, x= 's',y='SwivelAngle', hue='Trial',palette=mpl.colormaps['gist_rainbow'])

# # STD
# fig8a = plt.figure("Idea 8 - STD", figsize=[4,4])
# ax8a1 = fig8a.add_subplot(111)
# ax8a1.set_title("STD")
# std_df = pd.DataFrame(columns=['s','STD Swivel','Subject'])
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     for s in s_to_plot:
#         # s=s.round(s_round)
#         s_df = group_df_subject[group_df_subject['s']==s]
#         std = np.std(s_df['SwivelAngle'])
#         df = pd.DataFrame({'s':[s],'STD Swivel':[std], 'Subject':[subject]})
#         std_df = pd.concat([std_df, df], ignore_index=True)
#         print("s = ",s,"\tstd = ",std)
#     print("STD",std_df)
# # ax8a1.plot(std_df)
# sns.lineplot(data=std_df, x='s',y='STD Swivel',hue='Subject')

# # Mean
# fig8b = plt.figure("Idea 8 - Mean", figsize=[4,4])
# ax8b1 = fig8b.add_subplot(111)
# ax8b1.set_title("Mean")
# mean_df = pd.DataFrame(columns=['s','Mean Swivel','Subject'])
# for subject in subject_list:
#     group_df_subject = group_df_full[group_df_full['Subject']==subject]
#     for s in s_to_plot:
#         # s=s.round(s_round)
#         s_df = group_df_subject[group_df_subject['s']==s]
#         mean = np.mean(s_df['SwivelAngle'])
#         df = pd.DataFrame({'s':[s],'Mean Swivel':[mean], 'Subject':[subject]})
#         mean_df = pd.concat([mean_df, df], ignore_index=True)
#         print("s = ",s,"\tmean = ",mean)
#     print("Mean",mean_df)
# # ax8b1.plot(mean_df)
# sns.lineplot(data=mean_df, x='s',y='Mean Swivel',hue='Subject')


# Combine mean and STD in lineplot
fig8c = plt.figure("Swivel angle mean values with STD", figsize=[4,4])
ax8c1 = fig8c.add_subplot(111)
ax8c1.set_title("Mean Swivel Angle with with STD area ")
sns.lineplot(data=group_df_full, x= 's',y='SwivelAngle', hue='Subject', errorbar='sd')
ax8c1.set_xlabel("s")
ax8c1.set_ylabel("Mean Swivel angle (°)")


# plt.figure(999)
# plt.plot(group_df_full[group_df_full['Subject']=="2682"][["s"]].values,group_df_full[group_df_full['Subject']=="2682"][["SwivelAngle"]].values)
# plt.plot(group_df_full[group_df_full['Subject']=="1543"][["s"]].values,group_df_full[group_df_full['Subject']=="1543"][["SwivelAngle"]].values)
# fig2 = plt.figure(999,figsize=[4,4])
# sns.JointGrid(data=group_df_full,x="s", y="SwivelAngle", hue='Subject')
# fig2 = plt.figure(1000,figsize=[4,4])
# g=sns.violinplot(data=group_df_full,x="s", y="TwistAngle", hue='Subject')
# g = sns.relplot(data=group_df_full,x="s", y="TwistAngle", hue='Subject')
######################################################################
##################################################################################
# fig3 = plt.figure(figsize=[4,4])
# ax31 = fig3.add_subplot(211)
# ax31 = sns.scatterplot(data=group_df , x='s', y='TwistAngle', hue='Trial', palette=mpl.colormaps['autumn'], legend='brief',edgecolor="none")
# ax31.set_xlabel('Abscissa S')
# ax31.set_ylabel('Twist Angle (°)')
# ax31.set_title('Twist Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')
# ax32 = fig3.add_subplot(212)
# ax32 = sns.scatterplot(data=group_df , x='s', y='SwivelAngle', hue='Trial', palette=mpl.colormaps['autumn'], legend='brief',edgecolor="none")
# ax32.set_xlabel('Abscissa S')
# ax32.set_ylabel('Swivel Angle (°)')
# ax32.set_title('Swivel Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')


##################################################################################
# fig3 = plt.figure(figsize=[4,4])
# ax31 = fig3.add_subplot(211)
# ax31 = sns.scatterplot(data=group_df , x='s', y='TwistAngle', hue='Trial', palette=mpl.colormaps['autumn'], legend='brief',edgecolor="none")
# ax31.set_xlabel('Abscissa S')
# ax31.set_ylabel('Twist Angle (°)')
# ax31.set_title('Twist Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')
# ax32 = fig3.add_subplot(212)
# ax32 = sns.scatterplot(data=group_df , x='s', y='SwivelAngle', hue='Trial', palette=mpl.colormaps['autumn'], legend='brief',edgecolor="none")
# ax32.set_xlabel('Abscissa S')
# ax32.set_ylabel('Swivel Angle (°)')
# ax32.set_title('Swivel Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')



##################################################################################

# fig5 = plt.figure(figsize=[4,4])
# ax51 = fig5.add_subplot(211)
# ax51 = sns.scatterplot(data=group_df , x='Trial', y='TwistAngle', hue='s', palette=mpl.colormaps['tab20'], legend='full',edgecolor="none")
# ax51.set_xlabel('Trial')
# ax51.set_ylabel('Twist Angle (°)')
# ax51.set_title('Twist Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')
# ax52 = fig5.add_subplot(212)
# ax52 = sns.scatterplot(data=group_df , x='Trial', y='SwivelAngle', hue='s', palette=mpl.colormaps['tab20'], legend='full',edgecolor="none")
# ax52.set_xlabel('Trial')
# ax52.set_ylabel('Swivel Angle (°)')
# ax52.set_title('Swivel Angle of subject n°'+subject+' depending on the trial and the position on the wireloop')





# except:
#     print('Could not find RT for trial n°', trial_str)

plt.show()

