campaign_name="Mover202207"
subject="1543"
file_prefix="Mover20220728_$subject""_Xsens_0_"
file_suffix="-001"

echo 'Default paths for data are set by the folder: /home/raphael/Documents/MOVER/Data/Mover202207/'
echo 'To modify this path, please, go to the used programs and change them by hand'
for trial in {1..50}
do
	# python3 IK.py $subject $trial
	python3 JointsCenters.py $subject $trial
done
#rm -rf ./*.mvnx
