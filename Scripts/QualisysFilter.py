##################################################################################
# Libraries

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
sns.set_theme(style="darkgrid")

import sys

from scipy import signal

##################################################################################
# TEST Part: juste to debug this programm as a standalone 
# Define subject and trial to compute
campaign_name = 'Mover202207'
subject = '1543'
trial = '0001'
sys.argv.append(campaign_name)
sys.argv.append(subject)
sys.argv.append(int(trial))
##################################################################################

# Listen .sh parameters
if(len(sys.argv) == 1):
    print("Please provide the campaign name, the subject name and the trial")
    exit()
elif(len(sys.argv) == 2):
    print("Please provide the subject name and the trial")
    exit()
elif(len(sys.argv) == 3):
    print("Please provide the trial")
    exit()
else:
    campaign_name = sys.argv[1]
    subject = sys.argv[2]
    trial = int(sys.argv[3])
    trial_str = f'{trial:04d}'


##################################################################################
## Files
# Input files
input_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full_clean.trc'

# Output files
# output_mocap_filename = '/home/raphael/Documents/MOVER/Data/Mover202207/Qualisys/'+subject+'/'+trial_str+'_full_filtered.trc'
output_mocap_filename = input_mocap_filename[:-4]+'_filtered.trc'

##################################################################################
# Data parameters
v_human = 0.025 #[m/s]
qualisys_frequency = 100 #[Hz]

##################################################################################
# Open .trc file from Qualisys

## Extract Data from the .trc file and split them in multiple dataframes
print("\nExtracting Data from .trc file: ", input_mocap_filename)
# Extract first row of the .trc file
print('- Extract 1st line')
header_l1_trc = pd.read_csv(input_mocap_filename, nrows=1, header = None, delimiter='\t')

# Extract parameter header
print('- Extract Parameters')
parameters = pd.read_csv(input_mocap_filename, nrows=1, header = 1, skiprows=0, delimiter='\t')

# if data in .trc file are expressed in [mm], we have to convert joints center also in [mm] (multiply by 1000): the data from OSimModel.py are expressed in [m]
unit_multiplier = 1 
if parameters['Units'][0] =='mm': 
    unit_multiplier = 1000
    print('\tWARNING: Data in .trc file are expressed in [mm]')

# Extract markers names of the .trc file
print('- Extract markers names')
markers_header = pd.read_csv(input_mocap_filename, nrows=1 , header = None ,skiprows=3) ## make just one celle tab --> use it only for export
markers_trc = pd.read_csv(input_mocap_filename, nrows=2 , header = None , delimiter='\t',skiprows=3) # multi cells tab, but contains NaN values
print("markers names\n", markers_trc)
# Avoid NaN in the markers names dataframe, by replacinf NaN with the marker name
for col_idx in range(markers_trc.shape[1]-1):
    if pd.isna(markers_trc.iloc[0,col_idx]) and pd.isna(markers_trc.iloc[0,col_idx+1]):
        markers_trc.iloc[0,col_idx] = markers_trc.iloc[0,col_idx + 2]
        markers_trc.iloc[0,col_idx+1] = markers_trc.iloc[0,col_idx + 2]

print("markers names\n", markers_trc)
# Extract dataframe containing coordinates of the markers
print('- Extract data')
data_trc = pd.read_csv(input_mocap_filename, header = 1, delimiter='\t',skiprows=3)
##################################################################################
# FILTER
##################################################################################

# print("Data:\n",data_trc)

print("Copying dataframe")
data_trc_filtered = data_trc.copy()

##################################################################################
# FILTER PARAMETERS
filter_order = 4
cuttoff_f = 50
b, a = signal.butter(filter_order, cuttoff_f, "low", output='ba', fs=1000) # Parametrize the filter : order = 4, cuttoff frequency = 50 Hz, filter output: 'ba' = numerator/denominator,, original signal frequency

##################################################################################
# # TEST PART
# # Try the filter on 1 column:
# data_trc_test = data_trc.copy()
# marker_test = 'X1'
# data_trc_test[marker_test] = signal.filtfilt(b, a, data_trc_test[marker_test])
# # Compute error
# err_filt = abs(data_trc_test[marker_test]-data_trc[marker_test])

# data_trc_test.rename(columns={'Unnamed: 0':'Frame','Unnamed: 1':'Time'}, inplace=True)
# print("Data:\n",data_trc_test)
# figure = plt.figure(figsize=[4,4])
# ax = figure.add_subplot(211)
# ax.set_title('Try the lowpass filter of order '+str(filter_order)+' with cuttoff frequency='+str(cuttoff_f)+' Hz - Subject n°'+str(subject)+' - Trial n°'+trial_str+' - Marker '+marker_test)
# ax.plot(data_trc_test['Time'], data_trc[marker_test], label='raw')
# ax.plot(data_trc_test['Time'], data_trc_test[marker_test], label='filtered', linestyle='solid')
# ax.set_ylabel('Value coordinate (mm)')
# ax2 = figure.add_subplot(212)
# ax2.plot(data_trc_test['Time'], err_filt, label='Err', linestyle='solid')
# ax2.set_xlabel('Time (s)')
# ax2.set_ylabel('Error (mm)')
# ax.legend()
# plt.show()

##################################################################################
# FULL COMPUTATION PART
flag_fail = False
print('Filter each column:')
for col in data_trc_filtered.columns[2:]:
    try:
        data_trc_filtered[col] = signal.filtfilt(b, a, data_trc[col]) # Filter each column
        print('Subject n°',subject,' - Trial n°',trial_str," - Column ",col,' filtered')
    except:
        print('Subject n°',subject,' - Trial n°',trial_str," - Column ",col,' failed')
        flag_fail = True

if flag_fail == True:
    input("At least one column failed during filtering\n\t->Please Confirm by pressing any button\t")


##################################################################################
# Save new .trc file

# Replace Dirty columns names by null string ''
data_trc_filtered.rename(columns={'Unnamed: 0':'','Unnamed: 1':''}, inplace=True) 

## Reconsitute the .trc file
# Concatenate in the right order the 4 dataframes to obtain the .trc file like the original one
print('\nSaving the new .trc file...')
header_l1_trc.to_csv(output_mocap_filename, index=False, sep='\t', header=False)
parameters.to_csv(output_mocap_filename, mode="a", index=False, sep='\t', header=True) # mode "a" means to append dataframe in the .csv file
#  Fill the header markers names
markers_header.to_csv(output_mocap_filename, mode="a", index=False, sep=' ', header=False) # a simple ' ' [space] string works perfectly to transform the 1 cell dataframe into a multi-cells csv file (the \t are kept in the one cell df)
# Fill the dataframe with new values
data_trc_filtered.to_csv(output_mocap_filename, mode="a", index=False, sep='\t', header=True)
print('\t-> Data saved in ',output_mocap_filename)



