from cProfile import label
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme()

from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

import os, sys

import resample


####################################################
# Useful functions
def Distance(x1,y1,z1,x2,y2,z2):
    return np.sqrt((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 )


####################################################

# path description
def x(s):
    x = 0.5
    return x
def y(s):
    y = 0.4*s - 0.2
    return y
def z(s):
    z = 0.35 - 0.6*(s - 0.5)**2
    return z

# Curvilinear Abscissa Function
def curv_abscissa(markers_csv_path, ros_file_path, curv_abs_file_path, power_resolution = 5):

    # Import Dataframe
    markers_df = pd.read_csv(markers_csv_path, delimiter=',', header = 0)
    # print(markers_df)

    # Identify HANC (center point of the handle, in the middle of HANF and HANB) 
    markers_df['HANC_x'] = (markers_df['HANF_x'] + markers_df['HANB_x']) / 2
    markers_df['HANC_y'] = (markers_df['HANF_y'] + markers_df['HANB_y']) / 2
    markers_df['HANC_z'] = (markers_df['HANF_z'] + markers_df['HANB_z']) / 2

    # # Identify the characteristics of the Qualisys data
    # print("X_0 real = ", markers_df['HANC_x'].iat[0],"\tdX = ", markers_df['HANC_x'].iat[-1]- markers_df['HANC_x'].iat[0])
    # print("Y_0 real = ", markers_df['HANC_y'].iat[0],"\tdY = ", markers_df['HANC_y'].iat[-1]- markers_df['HANC_y'].iat[0])
    # print("Z_0 real = ", markers_df['HANC_z'].iat[0],"\tdZ = ", markers_df['HANC_z'].iat[-1]- markers_df['HANC_z'].iat[0])

    # Create dataframe containing only handle center coordinates, reals and recalibrated. 
    # To corresponds to theoretical fonctions, let's reassociate the columns between real and theorical: X_theo = Y_real, Y_theo = -Xreal, Z_theo = Z_real
    # Convert [mm] in [m]
    new_qualisys_df = pd.DataFrame()
    new_qualisys_df['x_real'] = markers_df['HANC_y'] /1000
    new_qualisys_df['y_real'] = -markers_df['HANC_x'] /1000
    new_qualisys_df['z_real'] = markers_df['HANC_z'] /1000
    new_qualisys_df['Time'] = markers_df['Time']


    # print("\nX_0 new = ", new_qualisys_df['x_real'].iat[0],"\tdX new = ", new_qualisys_df['x_real'].iat[-1]- new_qualisys_df['x_real'].iat[0])
    # print("Y_0 new = ", new_qualisys_df['y_real'].iat[0],"\tdY new = ", new_qualisys_df['y_real'].iat[-1]- new_qualisys_df['y_real'].iat[0])
    # print("Z_0 new = ", new_qualisys_df['z_real'].iat[0],"\tdZ new = ", new_qualisys_df['z_real'].iat[-1]- new_qualisys_df['z_real'].iat[0])

    # Identify the highest point on z axis: reference to align the observed path to the theoretical path
    idx_z_max_hanc = new_qualisys_df['z_real'].idxmax()
    # print("Id HANC_z max real: " , idx_z_max_hanc)

    # Get coordinates of the highest point point on z axis
    HANC_x_max = new_qualisys_df['x_real'][idx_z_max_hanc]
    HANC_y_max = new_qualisys_df['y_real'][idx_z_max_hanc]
    HANC_z_max = new_qualisys_df['z_real'][idx_z_max_hanc]
    # print("Highest point is in [",HANC_x_max,", ",HANC_y_max,", ",HANC_z_max,"]")


    ########################################################################
    # Compute Curvilinear Abscissa 
    ########################################################################

    # Hardcoded from path builder in C++
    nb_cut= 10**power_resolution    # Define the resolution of the path
    s = np.linspace(0.0,1.0,nb_cut)
    s=s.round(power_resolution) # Clean the linspace which sometimes give hazardous results
    print("S resolution = ",nb_cut,' points')
    # print("s = ",s)

    # Index of the highest point on z-axis, which will become the reference point for the theoretical parabolic curve (which contains ONE maximum)
    idx_z_max_theo = z(s).argmax()
    # print("Id HANC_z max theoretical : " , idx_z_max_theo)
    # print("Highest point is in [",x(s[idx_z_max_theo]),", ",y(s[idx_z_max_theo]),", ",z(s[idx_z_max_theo]),"]")
    # print("X_0 real = ", x(0),"\tdX = ", x(s[-1])- x(0))
    # print("Y_0 real = ", y(0),"\tdY = ",y(s[-1])- y(0))
    # print("Z_0 real = ", z(0),"\tdZ = ", z(s[-1])- z(0))


    # Compute error Real - Theo:
    delta_x_real_theo = HANC_x_max - x(s[idx_z_max_theo]) 
    delta_y_real_theo = HANC_y_max - y(s[idx_z_max_theo]) 
    delta_z_real_theo = HANC_z_max - z(s[idx_z_max_theo]) 
    # print('\nErreurs:\nx: ', delta_x_real_theo,'\ny: ', delta_y_real_theo,'\nz: ', delta_z_real_theo)

    # Put offsets on Qualisys recalibrated data:
    new_qualisys_df['x_moved'] = new_qualisys_df['x_real'] - delta_x_real_theo 
    new_qualisys_df['y_moved'] = new_qualisys_df['y_real'] - delta_y_real_theo
    new_qualisys_df['z_moved'] = new_qualisys_df['z_real'] - delta_z_real_theo

    # # Verify that max_z_points have the same coordinates in robot frame:
    # # Get coordinates of the highest point point on z axis
    # HANC_x_max_modified = new_qualisys_df['x_moved'][idx_z_max_hanc]
    # HANC_y_max_modified = new_qualisys_df['y_moved'][idx_z_max_hanc]
    # HANC_z_max_modified = new_qualisys_df['z_moved'][idx_z_max_hanc]

    # print("\nVerify if Theoretical data is equal to modified Qualisys data:")
    # print("Highest point for modified Qualisys is in [",HANC_x_max_modified,", ",HANC_y_max_modified,", ",HANC_z_max_modified,"]")
    # print("Highest point for theoretical data is in [",x(s[idx_z_max_theo]),", ",y(s[idx_z_max_theo]),", ",z(s[idx_z_max_theo]),"]")


    # S COMPUTATION
    s_df = pd.DataFrame()
    for i in range (new_qualisys_df.shape[0]):
        percentage=(i/new_qualisys_df.shape[0]*100)
        print(f'Computing S: {percentage:.2f} %', end='\r')
        found = False
        j=0
        s_sol = 0
        x_real = new_qualisys_df['x_moved'][i]
        y_real = new_qualisys_df['y_moved'][i]
        z_real = new_qualisys_df['z_moved'][i]
        t = new_qualisys_df['Time'][i]
        d_min = Distance(x(s_sol),y(s_sol),z(s_sol),x_real,y_real,z_real)
        for s_j in s:
            d_test = Distance(x(s_j),y(s_j),z(s_j),x_real,y_real,z_real)
            if d_test < d_min:
                s_sol = s_j
                d_min = d_test
        # print('s_sol = ',s_sol, '\t', i,'/',new_qualisys_df.shape[0])
        s_df = pd.concat([s_df,pd.DataFrame({'Time':[t],'s':[s_sol]})], ignore_index=True)
    # print(s_df)


    # Export S dataframe
    print("Exporting...")
    s_df.to_csv(curv_abs_file_path)
    print("\t-> Data exported to:",curv_abs_file_path )


    ############################
    #  Vérif S corrélation between ROS and Qualisys acquisitions

    # Load ROS Curvilinear Abscissa
    ## Import Dataframe
    ros_df = pd.read_csv(ros_file_path, delimiter=',', header = 0)
    # Define S and Time column name in ROS df
    s_col_name = '/torque_qp/panda_rundata/s_curr'
    time_col_name = '__time'
    # Clean NaN values
    s_ros_df = ros_df[ros_df[s_col_name].notnull()][[s_col_name, time_col_name]].reset_index()
    # Set ROS time to origin:
    s_ros_df[time_col_name] = s_ros_df[time_col_name] - s_ros_df[time_col_name][0]

    # resample ROS Data to correspond to Qualisys data acquisition
    s_ros_df = resample.Resample(s_ros_df, time_col_name, f_out = 100) # Qualisys data is sampled at 100Hz

   

    # print(s_ros_df[s_col_name])
    # print(s_df['s'])


    # Compute correlation
    corr = s_df['s'].corr(s_ros_df[s_col_name])
    # print("\nCorrelation coeff. (Pearson):", corr) 
    
    # Plot results
    # figure = plt.figure(figsize=[4,4])
    # ax = figure.add_subplot(111)
    # ax.plot(s_df['Time'],s_df['s'], color = 'blue', label='S computed')
    # ax.plot(s_ros_df[time_col_name]-s_ros_df[time_col_name][970],s_ros_df[s_col_name], color = 'red', label='S from robot')
    # ax.set_xlabel('Time')
    # ax.set_ylabel('S')
    # ax.legend()
    # ax.set_title('Curvilinear Abscissas S computed from observation vs. given by the robot - Correlation (Pearson\'s):'+str(np.round(corr,6)))
    # print("The offset on the graph doesn't have an ipact on the correlation coefficient")
    # plt.show()

    # Return resulting dataframe and corrélation
    return s_df, corr

