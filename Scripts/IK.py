##################################################################################
# Libraries
import opensim as osim

##########################################
# IMPORTANT
##########################################
# Please, scale the model first, with a python script, or directly in the OpenSIM GUI on Windows
print('\nDid you scale your model ?\n')

##################################################################################

def IK_osim(model_path, mocap_file_path, IK_setup_file_path, mot_file_path):
    ##################################################################################
    ## Computing
    # Define Model
    my_model = osim.Model(model_path)

    # Update state of the model (used to lock joints below)
    state = my_model.initSystem()

    # Locking Back joint 
    for coord in ['lumbar_extension', 'lumbar_bending', 'lumbar_rotation']: 
        c = my_model.getCoordinateSet().get(coord)
        c.set_default_value(c.getValue(state))
        c.set_locked(True)

    ## Create IK tool using Setup file (.xml)
    IK_tool = osim.InverseKinematicsTool(IK_setup_file_path)
    IK_tool.setModel(my_model)

    # Get initial and final time 
    markerData = osim.MarkerData(mocap_file_path)
    initial_scale_time = markerData.getStartFrameTime()
    final_scale_time = markerData.getLastFrameTime()
    IK_tool.setStartTime(initial_scale_time)
    IK_tool.setEndTime(final_scale_time)

    # Define trial to solve
    IK_tool.setMarkerDataFileName(mocap_file_path)

    # Define output file
    IK_tool.setOutputMotionFileName(mot_file_path)

    ##################################################################################
    # Execute IK
    IK_tool.run()


